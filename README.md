## To create tarball for submission
ant -buildfile src/build.xml tarzip

CS542 Design Patterns
Spring 2016
PROJECT 2 README FILE

Due Date: Monday, February 29, 2016
Submission Date: Monday, February 29, 2016
Grace Period Used This Project: 0 Days
Grace Period Remaining: 3 Days
Author(s): Gabrielle Winterton and Kevin Travers
e-mail(s): gwinter1@binghamton.edu and ktraver1@binghamton.edu

PURPOSE:

[
  This project takes an input file, number of threads, and debug value as parameters and then creates the correct number of threads so that they can calculate the sum of all the prime numbers in the input file given. (In this case any non-even number is considered prime)

  We chose to use a vector as the results data structure because it is already syncrognized and the structure will need to increase its size less times to accomidate the large input file 

]

PERCENT COMPLETE:

[
 We believe that we have completed 100% of this project.
]

PARTS THAT ARE NOT COMPLETE:

[
  We believe that we completed 100% of this project.
]

BUGS:

[
  None
]

FILES:

[
  Included with this project are 10 files:

  Driver.java, the main file of the program that contains main
  Results.java, the file that stores the prime numbers and calculates the sum.
  StdoutDisplayInterface.java, the interface that has the function to write the sum to the screen
  CreateWorkers.java, file that creates the worker threads
  WorkerThread.java, file that tells each worker thread what to do
  FileProcessor.java, the file that reads an input file one line at a time
  IsPrime.java, the file responsible for determining if a number is prime, in this case a number is prime if the number is not even
  Logger.java, the file that displays messages to the screen depending on the debug level of the program running
  build.xml, the file that assists in compiling and running the code
  README.txt, the text file you are presently reading
]

SAMPLE OUTPUT:

[
  g7-30:~/Desktop/design_patterns/Gabrielle_Winterton_Kevin_Travers/primeThreads> ant -buildfile src/build.xml run -Darg0=input.txt -Darg1=3 -Darg2=0
Buildfile: /import/linux/home/ktraver1/Desktop/design_patterns/Gabrielle_Winterton_Kevin_Travers/primeThreads/src/build.xml

jar:

run:
     [java] The sum of all the prime numbers is: 22500

BUILD SUCCESSFUL
Total time: 1 second
g7-30:~/Desktop/design_patterns/Gabrielle_Winterton_Kevin_Travers/primeThreads> 
]

TO COMPILE:

[
  Just extract the files and then do a "make". 
  Assuming you are in the directory containing this README:
  ant -buildfile src/build.xml all
]

TO RUN:

[
  Document here how your TA can test your program after extracting your gzipped-
  tarball. Be as clear as possible, refer to "... FOR DUMMIES" book format.
  Assume your TA is completely lost, there is another universe. Here is a
  sample:

  Please run as: ant -buildfile src/build.xml run <INPUT FILE> <# OF THREADS> <DEBUG VALUE>
  For example:   ant -buildfile src/build.xml run -Darg0=input.txt -Darg1=3 -Darg2=2
]

EXTRA CREDIT:

[
  N/A
]


BIBLIOGRAPHY:

This serves as evidence that we are in no way intending Academic Dishonesty.
Gabrielle Winterton and Kevin Travers

[
  No outside resources were used
]

ACKNOWLEDGEMENT:

[

  During the coding process we did not talk to anyone else about the project

]