
package primeThreads.driver;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import primeThreads.util.FileProcessor;
import java.io.BufferedReader;
import primeThreads.util.IsPrime;
import primeThreads.store.Results;
import primeThreads.threadMgmt.CreateWorkers;
import primeThreads.util.Logger;
public class Driver{
//does everything
	public static void main(String args[]) {

		//System.out.println(args[2]);
		//check if not enought arguments are entered
//checks the input
		if (args[2].equals("${arg2}")){
			System.err.println("Not enough arguments: ant -buildfile src/build.xml run -Darg0=input.txt -Darg1=(1 to 5) -Darg2=(0 to 4)");
			System.exit(0);
		}else if(!args[3].equals("${arg3}")){
			System.err.println("Too many arguments: ant -buildfile src/build.xml run -Darg0=input.txt -Darg1=(1 to 5) -Darg2=(0 to 4)");
			System.exit(0);
		}
		
		//input threads and debug
				
		try{
			//check the thread and delooger value 
			int threadCount = Integer.parseInt(args[1]);
			if((0 >= threadCount)||(5<threadCount)){
				System.err.println("Thread count needs to be 1 to 5");
			System.exit(0);
			}
			int delogger = Integer.parseInt(args[2]);
			if((0 > delogger)||(4<delogger)){
				System.err.println("Delogger needs to be 0 to 4");
			System.exit(0);
			}
//opens file and checks if file is there
			File file = new File(args[0]);
			if(!file.isFile()){
				System.err.println("File entered does not exist");
				System.exit(0);
			}
//set deloger values
			Logger.setDebugValue(delogger);
			//System.out.println(Logger.DebugLevel.CONSTRUCTOR);
			Logger.writeMessage("BufferedReader constructor called", 	Logger.DebugLevel.CONSTRUCTOR);

			BufferedReader  br = new BufferedReader(new FileReader(file));
			//FileProcessor processor = new FileProcessor();
			Logger.writeMessage("FileProcessor constructor called", Logger.DebugLevel.CONSTRUCTOR);
			FileProcessor processor = new FileProcessor();
			Logger.writeMessage("Results constructor called", Logger.DebugLevel.CONSTRUCTOR);
			Results myResults =  new Results();
			Logger.writeMessage("IsPrime constructor called", Logger.DebugLevel.CONSTRUCTOR);
			IsPrime p = new IsPrime();
			Logger.writeMessage("CreateWorkers constructor called", Logger.DebugLevel.CONSTRUCTOR);
			CreateWorkers myWorker = new CreateWorkers();
			myWorker.startWorkers(threadCount,br,myResults,processor,p);
			Logger.writeMessage(myResults.getNumbers(), Logger.DebugLevel.DATAPRINT);
			myResults.writeSumToScreen();
			
		}catch(IOException e){
			System.err.println("could not access file exception thrown:"+e);
			System.exit(0);
		}catch (NumberFormatException e){
			System.err.println("Use ints for delogger and thread count excption thrown:"+e);
			System.exit(0);
		}catch(Exception e){
			System.err.println("Excption thrown:"+e);
			System.exit(0);
		}

		
		
	} // end main(...)
/*
	public static File openFile(String fileName){
		File file = new File(fileName);
		if(!file.isFile()){
			System.err.println("File entered does not exist");
			System.exit(0);
		}
		return file;
	}*/

} // end public class Driver

