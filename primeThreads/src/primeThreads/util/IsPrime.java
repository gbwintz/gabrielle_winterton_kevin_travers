
package primeThreads.util;

public class IsPrime {

	//Prime class considers all non-even numbers to be prime
//returns whether or not the input is prime
	public boolean isPrime(int num){
		if(num % 2 == 0){
			return false;
		}
		else{
			return true;
		}
		//hopefully this works
	}

}
