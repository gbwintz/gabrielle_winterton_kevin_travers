
package primeThreads.store;

import java.util.Vector;

public class Results implements StdoutDisplayInterface {
    // appropriate data structure as private data member
    private Vector<Integer> numbers = new Vector<Integer>();


    // appropriate method to save prime number to the data structure
    public void storePrime(int n){
	numbers.addElement(n);
    }
//returns the sum of all the primes(odds) in vector
    public int calculateSum(){
	int sum = 0;
	for(int i = 0; i < numbers.size(); i ++){
		sum += numbers.get(i);
	}
	return sum;
    }
	//writes the clculated sum to the screen final output
    public void writeSumToScreen() {
	int sum = calculateSum();
	System.out.println("The sum of all the prime numbers is: " + sum);
    }
//returns all the values in the vector as a string
    public String getNumbers(){
	return numbers.toString();
    }
} 


