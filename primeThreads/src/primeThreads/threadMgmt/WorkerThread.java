
package primeThreads.threadMgmt;
import primeThreads.util.FileProcessor;
import java.io.BufferedReader;
import primeThreads.util.IsPrime;
import primeThreads.store.Results;
import primeThreads.util.Logger;
public class WorkerThread implements Runnable{
	private	BufferedReader inputFile;
	private Results threadResults;
	private FileProcessor threadProcessor;
	private IsPrime threadPrime;
	//thread constructor which set all instacne for this thread to work on
 	public WorkerThread(BufferedReader file, Results myResults, FileProcessor processor, IsPrime prime){
		inputFile = file;
		threadResults = myResults;
		threadProcessor = processor;
		threadPrime = prime;
	}
	//run will run the thread and go through the text file line by line and add all primes(odds) to vector
	public void run(){
		Logger.writeMessage("Run called", Logger.DebugLevel.THREADRUN);
		boolean isPrime = false;

		String lineRead = "";
		while(lineRead != null){
			
				lineRead = threadProcessor.readLineFromFile(inputFile);
				if (lineRead != null){
					int num = Integer.parseInt(lineRead);
					isPrime = threadPrime.isPrime(num);
					if (isPrime){
						Logger.writeMessage("Prime added", Logger.DebugLevel.VALUEADD);
						threadResults.storePrime(num);
					}
				}
				
			
		}

		
		
	
	}
    

}
