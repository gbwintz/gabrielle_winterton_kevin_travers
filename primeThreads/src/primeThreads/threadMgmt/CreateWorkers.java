
package primeThreads.threadMgmt;
import primeThreads.util.FileProcessor;
import java.io.BufferedReader;
import primeThreads.util.IsPrime;
import primeThreads.store.Results;
import primeThreads.util.Logger;
public class CreateWorkers  {
    // this class has the method startWokers(...)
	//creates num amount of threads
	//this will create threads according threadnum
	public void startWorkers(int threadNum, BufferedReader file, Results myResults, FileProcessor processor, IsPrime prime){	
		for (int i = 0; i < threadNum; i++){
			try{
				Logger.writeMessage("Thread constructor called", 	Logger.DebugLevel.CONSTRUCTOR);
				Thread myThread = new Thread(new WorkerThread(file, myResults, processor, prime), "Thread number"+i);
				myThread.start();
				myThread.join();
				
			}catch(Exception e){
				System.err.println("Exception Thrown:"+e);
			}
		}
		
		
	}
}

